#include <iostream>
#include <vector>
#include <sys/time.h>
#include <string.h>

using namespace std;

// Definiciones
struct solucion {
	int cMin;
	int cant;
};

struct minPos {
	int minimo;
	int posicion;
};

// La funcion que encuentra la distribucion de trabajos con costo minimo.
solucion distribucionOptima(int n, vector< vector< int > >& costos, vector< int >& trabajos);
// Auxiliar para calcular el minimo
minPos minYPos(const vector<int>& origen);

// Funciones y variables globales para la medicion de tiempo de ejecucion
timeval start, end;
void init_time() {
	gettimeofday(&start, 0);
}

double get_time() {
	gettimeofday(&end, 0);
	return (1000000 * (end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec)) / 1000000.0;
}

int main(int argc, char** argv) {
	bool medirTiempo = false;
	if(argc >= 2 && strcmp(argv[1], "tiempo") == 0) {
		medirTiempo = true;
	}
	
	int n;
	cin >> n;
	
	while(n) {
		// Crear la matriz de costos.
		vector<vector<int> > costos(n + 1, vector<int> (n + 1));
		// Cargo los costos del input.
		for(int i = 1; i < n+1; i++) {
			for(int j = 0; j < i; j++) {
				cin >> costos[i][j];
			}
		}
		vector<int> trabajos(n);
		
		if(medirTiempo) {
			init_time();
			for(int i = 0; i < 1000; i++) {
				distribucionOptima(n, costos, trabajos);
			}
			
			// tiempo en segundos
			double time = get_time();

			// como el tiempo esta en segundos, y lo queremos en milisegundos
			// habría que multiplicarlo por 1000, y despues, como queremos el
			// promedio, habría que dividirlo por 1000, así que esta bien
			// mostrarlo así
			cout << fixed << "Tiempo de ejecucion: " << time << "ms" << endl;
		} else {
			solucion res = distribucionOptima(n, costos, trabajos);
			
			// Ahora imprimo todos los datos solicitados en stdout (Costo total, 
			// cantidad de trabajos en mi lista solucion, y los trabajos en cuestion).
			cout << res.cMin << " " << res.cant << " ";
			for(int t = res.cant-1; t >= 0; t--) {
				cout << trabajos[t] << " ";
			}
			cout << endl;
		}
		
		cin >> n;
	}
	
	return 0;
}

solucion distribucionOptima(int n, vector< vector< int > >& costos, vector< int >& trabajos) {
	vector<vector<int> > costosMinimos(n + 1, vector<int> (n));
	// Array para reconstruir la solucion.
	vector<int> indices(n, 0);
	// Comienzo a completar los costosMinimos.
	costosMinimos[1][0] = costos[1][0];
	
	for(int i = 2; i < n+1; i++) {
		for(int j = 0; j < i-1; j++) {
			costosMinimos[i][j] = costosMinimos[i-1][j] + costos[i][i-1];
		}
		// Para todos el caso en que se puede llegar de varias maneras, calculamos la de costo menor.
		int min = costos[i][0] + costosMinimos[i-1][0];
		
		for(int k = 0; k < i-1; k++) {
			if(costos[i][k] + costosMinimos[i-1][k] < min) {
				min = costos[i][k] + costosMinimos[i-1][k];
				indices[i-1] = k;	// Me guardo el trabajo anterior.
			}
		}
		costosMinimos[i][i-1] = min;
	}
	// Una vez tengo los costos calculados, busco el mínimo en total y cual fue el trabajo anterior.
	minPos final = minYPos(costosMinimos[n]);
	// Y ahora reconstruyo la solucion. En nuestro caso, es la lista de trabajos
	// asignados a la maquina que proceso al trabajo n.
	int i = n, cant = 0;
	while(i > 0) {
		trabajos[cant] = i;
		cant++;
		if(final.posicion == i-1) {
			// Si los ultimos en cada maquina son consecutivos, tengo que ver de donde vino.
			i = indices[final.posicion];
			final.posicion = indices[i];
		} else {
			i--;
		}
	}
	
	solucion res = {final.minimo, cant};
	return res;
}

minPos minYPos(const vector<int>& origen) {
	int minimo = origen[0], posicion = 0;
	
	for(unsigned int j = 0; j < origen.size(); j++) {
		if(origen[j] < minimo) {
			minimo = origen[j];
			posicion = j;		// Guardo cual fue el ultimo en la otra maquina.
		}
	}
	minPos res = {minimo, posicion};
	return res;
}