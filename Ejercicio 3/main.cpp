#include <set>
#include <vector>
#include <iostream>
#include <string.h>
#include <sys/time.h>
using namespace std;

// Varios typedefs
typedef vector<vector<pair<int, int> > > Grafo;

// Prototipado de funciones
int Prim(Grafo& g, vector<int>& padre);
int EncontrarRutasOptimas(Grafo& g, int fabricas, vector<pair<int, int> >& aristas);

// Funciones y variables globales para la medicion de tiempo de ejecucion
timeval start, end;
void init_time()
{
	gettimeofday(&start, 0);
}

double get_time()
{
	gettimeofday(&end, 0);
	return (1000000 * (end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec)) / 1000000.0;
}

int inf = 0;

int main(int argc, char** argv) 
{
	bool medirTiempo = false;
	if(argc >= 2 && strcmp(argv[1], "tiempo") == 0)
	{
		medirTiempo = true;
	}

	while (true)
	{
		int f;
                int c;
		int r;

		cin >> f;
                
		if (f== 0)
			return 0;

                cin >> c;
		cin >> r;

		Grafo g(f + c + 1);
                
                for (int i = 0; i < f + c +1; i++)
		{
			g[i] = vector<pair<int, int> >();
		}
                
		inf = 0;

		int a, b, l;
		for (int i = 0; i < r; i++)
		{
			cin >> a;
			cin >> b;
			cin >> l;

			if (l + 1 > inf)
			{
				inf = l + 1;
			}

			g[a].push_back(pair<int, int>(l, b));
                        g[b].push_back(pair<int, int>(l, a));			
		}                            

		if (medirTiempo)
		{
			init_time();
			for (int i = 0; i < 1000; i++)
			{
                                vector<pair<int, int> > aristas;
				EncontrarRutasOptimas(g, f, aristas);
			}
			
			// tiempo en segundos
			double time = get_time();

			// como el tiempo esta en segundos, y lo queremos en milisegundos
			// habría que multiplicarlo por 1000, y despues, como queremos el
			// promedio, habría que dividirlo por 1000, así que esta bien
			// mostrarlo así
			cout << fixed << "Tiempo de ejecucion: " << time << "ms";
		}
		else
		{
                        vector<pair<int, int> > aristas;
                        int costoTotal = EncontrarRutasOptimas(g, f, aristas);
                        
			cout << costoTotal << " " << aristas.size();

			for (int i = 0; i < aristas.size(); i++)
			{
                                cout << " " << aristas[i].first << " " << aristas[i].second;
			}
		}

		cout << endl;
        } 

        return 0;
}

int Prim(Grafo& g, vector<int>& padre)
{        
	vector<int> minPeso(g.size());
	vector<bool> enHeap(g.size());

	for (int i = 0; i < g.size(); i++)
	{
		minPeso[i] = inf;
		padre[i] = 0;
		enHeap[i] = true;
	}

	minPeso[0] = 0;

	set<pair<int, int> > q;

	q.insert(pair<int, int>(0, 0));
	
	while (!q.empty())
	{
		pair<int, int> v = *(q.begin());
		q.erase(q.begin());
		
		if (minPeso[v.second] != v.first)
		{
			continue;
		}

		enHeap[v.second] = false;

		for (int j = 0; j < g[v.second].size(); j++)
		{
			int peso = g[v.second][j].first;
                        int u = g[v.second][j].second;
			
			if (enHeap[u] &&  minPeso[u] > peso)
			{
				padre[u] = v.second;
				minPeso[u] = peso;
				q.insert(pair<int, int>(peso, u));
			}
		}
	}
        
        int costoTotal = 0;

	for (int i = 1; i < g.size(); i++)
	{
		costoTotal += minPeso[i];
	}
        
        return costoTotal;
}

int EncontrarRutasOptimas(Grafo& g, int fabricas, vector<pair<int, int> >& aristas)
{
        for (int i = 1; i <= fabricas; i++)
        {
                g[0].push_back(pair<int, int>(0, i));
                g[i].push_back(pair<int, int>(0, 0));
        }
        
        vector<int> padre(g.size());
        int costo = Prim(g, padre);
                                
        for (int i = 0; i < padre.size(); i++)
        {
                if (padre[i] != 0 && padre[i] != i)
                {
                        aristas.push_back(pair<int, int>(i, padre[i]));                        
                }
        }
        
        return costo;
}