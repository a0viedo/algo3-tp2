#include <set>
#include <vector>
#include <queue>
#include <iostream>
#include <string.h>
#include <sys/time.h>
using namespace std;

// Varios typedefs
typedef vector<vector<pair<int, int> > > Grafo;

// Prototipado de funciones
int Prim(Grafo& g, vector<int>& padre);
int EncontrarMaster(Grafo& arbol);
vector<int> NodoMasLejanoConBFS(Grafo& arbol, int comienzo);

// Funciones y variables globales para la medicion de tiempo de ejecucion
timeval start, end;
void init_time()
{
	gettimeofday(&start, 0);
}

double get_time()
{
	gettimeofday(&end, 0);
	return (1000000 * (end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec)) / 1000000.0;
}

int inf = 0;

int main(int argc, char** argv) 
{
	bool medirTiempo = false;
	if(argc >= 2 && strcmp(argv[1], "tiempo") == 0)
	{
		medirTiempo = true;
	}

	while (true)
	{
		int n;
		int m;

		cin >> n;

		if (n == 0)
			return 0;

		cin >> m;

		Grafo g(n);
                Grafo arbol(n);

                for (int i = 0; i < n; i++)
		{
			g[i] = vector<pair<int, int> >();
			arbol[i] = vector<pair<int, int> >();
		}
                
		inf = 0;

		int a, b, c;
		for (int i = 0; i < m; i++)
		{
			cin >> a;
			cin >> b;
			cin >> c;

			if (c + 1 > inf)
			{
				inf = c + 1;
			}

			g[a - 1].push_back(pair<int, int>(c, b - 1));
                        g[b - 1].push_back(pair<int, int>(c, a - 1));			
		}                            

                      vector<int> padre(g.size());				
				Prim(g, padre);

				for(int i = 0; i < padre.size(); i++)
				{
					int j = padre[i];
				
					// Para encontrar el nodo master, no nos importan los pesos, solo las incidencias
					// así que le ponemos peso 1
					arbol[i].push_back(pair<int, int>(1 ,j));
					arbol[j].push_back(pair<int, int>(1 ,i));                
				}
		if (medirTiempo)
		{
			init_time();
			for (int i = 0; i < 1000; i++)
			{
	                        EncontrarMaster(arbol);
			}
			
			// tiempo en segundos
			double time = get_time();

			// como el tiempo esta en segundos, y lo queremos en milisegundos
			// habría que multiplicarlo por 1000, y despues, como queremos el
			// promedio, habría que dividirlo por 1000, así que esta bien
			// mostrarlo así
			cout << n << ";" << time;
		}
		else
		{			
                        vector<int> padre(g.size());
			int costoTotal = Prim(g, padre);
			
		        for(int i = 0; i < padre.size(); i++)
			{
				int j = padre[i];
				
				// Para encontrar el nodo master, no nos importan los pesos, solo las incidencias
				// así que le ponemos peso 1
				arbol[i].push_back(pair<int, int>(1 ,j));
				arbol[j].push_back(pair<int, int>(1 ,i));                
			}

                        int master = EncontrarMaster(arbol);
                        
			cout << costoTotal << " " << master + 1 << " " << g.size() - 1 << " ";

			for (int i = 1; i < padre.size(); i++)
			{
                                cout << i + 1 << " " << padre[i] + 1 << " ";
			}
		}

		cout << endl;
        } 

        return 0;
}

int Prim(Grafo& g, vector<int>& padre)
{        
	vector<int> minPeso(g.size());
	vector<bool> enHeap(g.size());

	for (int i = 0; i < g.size(); i++)
	{
		minPeso[i] = inf;
		padre[i] = 0;
		enHeap[i] = true;
	}

	minPeso[0] = 0;

	set<pair<int, int> > q;

	q.insert(pair<int, int>(0, 0));
	
	while (!q.empty())
	{
		pair<int, int> v = *(q.begin());
		q.erase(q.begin());
		
		if (minPeso[v.second] != v.first)
		{
			continue;
		}

		enHeap[v.second] = false;

		for (int j = 0; j < g[v.second].size(); j++)
		{
			int peso = g[v.second][j].first;
                        int u = g[v.second][j].second;
			
			if (enHeap[u] &&  minPeso[u] > peso)
			{
				padre[u] = v.second;
				minPeso[u] = peso;
				q.insert(pair<int, int>(peso, u));
			}
		}
	}

	int costoTotal = 0;

	for (int i = 1; i < g.size(); i++)
	{
		costoTotal += minPeso[i];
	}
        
	return costoTotal;
}

int EncontrarMaster(Grafo& arbol)
{        
        vector<int> caminoA = NodoMasLejanoConBFS(arbol, 0);
        vector<int> caminoB = NodoMasLejanoConBFS(arbol, caminoA.front());
        
        return caminoB[caminoB.size() / 2];
}

vector<int> NodoMasLejanoConBFS(Grafo& arbol, int comienzo)
{        
        vector<int> dist(arbol.size());
        vector<int> pred(arbol.size());
        for (int i = 0; i < pred.size(); i++)
        {
                dist[i] = -1;
                pred[i] = -1;
        }
        
        pred[comienzo] = 0;
                
        queue<pair<int, int> > lista;
        lista.push(pair<int, int>(comienzo, 0));
        
        while (!lista.empty())
        {
                int v = lista.front().first;
                int distancia = lista.front().second;
                
                dist[v] = distancia;
                
                distancia++;
                lista.pop();
                
                for (int i = 0; i < arbol[v].size(); i++)
                {
                        int u = arbol[v][i].second;
                        if (pred[u] == -1)
                        {
                                pred[u] = v;                                                       
                                lista.push(pair<int, int>(u, distancia));
                        }
                }
        }
        
        int nodoMasLejano = 0;
        
        for(int i = 0; i < dist.size(); i++)
        {
                if (dist[i] > dist[nodoMasLejano])
                {
                        nodoMasLejano = i;
                }
        }
        
        vector<int> camino;
        
        int actual = nodoMasLejano;
        camino.push_back(actual);
        
        while (actual != comienzo)
        {
                actual = pred[actual];
                camino.push_back(actual);
        }
                        
        return camino;
}
